# Word Ladder - Joshua Nissenbaum s2897359
#
# The aim of a word ladder is to replace one letter in the previous word so a new form is formed,
# without changing the positions of other letters and matching a supplied word in the dictionary word list.
# The shortest possible path between the starting and target word should be displayed.
#
# This Python program utilises the Breadth First Search algorithm to find the shortest path in the dictionary.
# Deque, popleft and defaultdict from https://docs.python.org/2/library/collections.html#collections.deque
#

import collections

# Find (Improved)
# Create a dictionary list of words which are connected whereby the removal of one character
# creates another word from the word list
def find(words, word_exceptions=None):
    dict = collections.defaultdict(list)  # List Dictionary (String)
    alphabet = 'abcdefghijklmnopqrstuvwxyz'  # The alphabet from A - Z

    if word_exceptions:
        for word in words:
            if word not in word_exceptions:
                for i in range(len(word)):
                    # Remove one character
                    one_dropped = word[:i] + word[i + 1:]
                    if one_dropped in words:
                        dict[word].append(one_dropped)
                        # print(one_dropped)
                    # Change one character using the supplied alphabet list
                    for letter in alphabet:
                        one_added = word[:i] + letter + word[i + 1:]
                        if one_added in words and one_added != word:
                            dict[word].append(one_added)
                            # print(one_added)

            # Add one character in the same position
            for i in range(len(word) + 1):
                for letter in alphabet:
                    new_word = word[:i] + letter + word[i:]
                    if new_word in words:
                        dict[word].append(new_word)
                        # print(new_word)
        return dict

    else:
        for word in words:
            for i in range(len(word)):
                # Remove one character
                one_dropped = word[:i] + word[i + 1:]
                if one_dropped in words:
                    dict[word].append(one_dropped)
                    # print(one_dropped)
                # Change one character using the supplied alphabet list
                for letter in alphabet:
                    one_added = word[:i] + letter + word[i + 1:]
                    if one_added in words and one_added != word:
                        dict[word].append(one_added)
                        # print(one_added)

            # Add one character in the same position
            for i in range(len(word) + 1):
                for letter in alphabet:
                    new_word = word[:i] + letter + word[i:]
                    if new_word in words:
                        dict[word].append(new_word)
                        # print(new_word)
        return dict


# Find Path
# Create a collection to map the relationships between similar words and utilise BFS to find the shortest path
def find_path(dict, start, target):
    word_collection = collections.deque([[start]])
    seen = set()

    # BFS (changed around and copied from the week 6 workshop on recursive DFS)
    while len(word_collection) != 0:
        current_path = word_collection.popleft()
        current_word = current_path[-1]
        # Target word reached, shortest path found
        if current_word == target:
            return current_path
        # Word has already been seen
        elif current_word in seen:
            continue

        seen.add(current_word)
        word_relations = dict[current_word]

        for word in word_relations:
            if word not in current_path:
                # Avoid loop cycles and re-mapping the same word
                word_collection.append(current_path[:] + [word])

    # A path has not been found from the entered starting word to target
    return None


# Prompt
def prompt(message):
    print("-------------------------------------------------------------------------")
    response = input(message)

    if response == "yes":
        return True
    else:
        return False


# Run Again
# Control flow and prompt for the user to access the main menu
def run_again():
    print("-------------------------------------------------------------------------")
    response = input("Would you like to run this program again? (yes/no)")

    if response == "yes":
        main_run()
    else:
        print("Goodbye for now.")
        exit()


# Add Exceptions Function
# Called from the main running function on a loop to build the word exceptions list as various inputs
def add_exceptions(words, word_exceptions, start, target):
    while True:
        word = input("Type the word and press enter. Type 'done' when you have finished adding words.")

        # User wishes to add their starting word to the word exceptions
        if word == start:
            print("You can't add your starting word to the exception list. Try again.")

        # User wishes to add their target word to the word exceptions
        elif word == target:
            print("You can't add your target word to the exception list. Try again.")

        # User has finished adding words, break the loop, continue with running
        elif word == "done":
            if prompt("Would you like to now find the shortest path for " + start + " and " + target + "?  (yes/no)"):
                print("Okay! Please wait... This could take a while.")
                dict = find(words, word_exceptions)
                run(words, dict, start, target)
            else:
                run_again()
                break

        # User has added a word, prompt to continue or finish/break loop
        else:
            word_exceptions.append(word)
            print("Word added! Keep adding or type 'done'.")


# Main Run Function
#
# Variables:
# start - User entered input for the beginning or start word
# target - User entered input for the end or target word
# words - Filtered dictionary word list based on start and target variables

def main_run():
    word_exceptions = []  # A set for the user to append exceptions to
    fname = input("Enter dictionary name: ")

    try:
        file = open(fname)
        lines = file.readlines()

    except IOError:
        print("Could not find that dictionary. Please make sure the file is in the local directory.")
        run_again()

    # Loop until the program has returned a True or False boolean state
    while True:
        start = input("Enter start word:")

        if len(start) == 0:
            print("You must enter a starting word!")
            main_run()

        target = input("Enter target word:")

        if len(target) == 0:
            print("You must enter a target word!")
            main_run()

        words = []
        for line in lines:
            word = line.rstrip()

            # Words of the same character length are filtered into a new words string array (performance)
            if len(word) == len(start):
                words.append(word)

        break

    # Same Character Length Control
    if len(start) != len(target):
        print("This program can only compare words of the same character length. For example: 'lead' -> 'gold'")
        run_again()

    if prompt("Would you like to add words that are not allowed to be used in the path? (yes/no)"):
        add_exceptions(words, word_exceptions, start, target)

    else:

        if prompt("Would you like to now find the shortest path for " + start + " and " + target + "?  (yes/no)"):
            print("Okay! Please wait... This could take a while.")
            dict = find(words)
            run(words, dict, start, target)
        else:
            run_again()


# Run
# Use the primary functions to execute the shortest path finding process
# Return the shortest path as a string of formatted steps
def run(words, dict, start, target):
    shortest_path = find_path(dict, start, target)  # The path returned as a list variable

    if shortest_path:
        # The path formatted as steps
        formatted_path = ['{:>3}' for word in shortest_path]
        p = ' -> '.join(formatted_path)

        print("-------------------------------------------------------------------------")
        print("Shortest Path: " + p.format(*shortest_path))
    else:
        print("-------------------------------------------------------------------------")
        print("Sorry, a path could not be found.")

    run_again()  # Main menu control prompt


# Run this source file as the primary program/module
if __name__ == '__main__':
    main_run()
